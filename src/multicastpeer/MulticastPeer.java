/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//
//Alunos: Giovanni de Oliveira Bandeira e Steven Justen Tunnicliff

//Observação: incluir no argumentos um endereço ip como 225.0.0.2

package multicastpeer;

import java.net.*;
import java.io.*;
import static java.lang.Math.random;
import java.util.Scanner;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
public class MulticastPeer{
    public static void main(String args[]) throws NoSuchAlgorithmException{ 
		// args give message contents and destination multicast group (e.g. "228.5.6.7")
		MulticastSocket s = null;
		try {
			InetAddress group = InetAddress.getByName(args[0]);//pega o endereço ip do grupo e o cria
			s = new MulticastSocket(6789);//cria o socket
			s.joinGroup(group);//entra no grupo

                        
                        Integer privateKey;//chave privada para a thread q vai ser criada
                        Integer publicKey;//chave pública ''
                        
                        privateKey = (int)(Math.random()*((4-1)+1))+1;//cria um número aleatório de 1 a 4 para servir de chave privada
                        publicKey = 5 - privateKey;
                        
 			byte [] m = ("Hello").getBytes();//cria a mensagem de entrada
			DatagramPacket messageOut = new DatagramPacket(m, m.length, group, 6789); //cria o datagrama que enviará a mensagem
			s.send(messageOut);//envia a mensagem
                        
                        Scanner scan = new Scanner(System.in);//cria o scanner que lerá o valor da thread pelo teclado
                        System.out.println("Digite o Valor(0 ou 1): ");
                        Integer value = scan.nextInt();//RECEBE O VALOR A SER UTILIZADO COMO BASE NO PHASE KING
                        
                        
                        Connection c = new Connection(s,group,value,privateKey,publicKey);//cria a thread
                        
                        while(c.isAlive()){//mantém o programa rodando enquanto a thread estiver viva
                            
                        }
                        
                        
			//s.leaveGroup(group);	//nao utilizado -> socket sai do grupo na propria thread	
		}catch (SocketException e){System.out.println("Socket: " + e.getMessage());
		}catch (IOException e){System.out.println("IO: " + e.getMessage());
		}finally {if(s != null) s.close();}
	}		      	
	
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------//
class Connection extends Thread {
	//DataInputStream in;
	//DataOutputStream out;
	private MulticastSocket socket;//socket
        private InetAddress group;//grupo
        private Integer value;//valor binário 
        private Integer ordem;//ordem de chegada da thread no grupo
	private Integer privateKey;//chave privada
	public Integer publicKey;//chave pública
        private Integer chaves[];//conjunto das chaves públicas das outras threads no grupo
        private Integer valores[];//conjunto de valores apresentados pelas threads
	public Connection (MulticastSocket s1, InetAddress group1,Integer value,Integer privK,Integer pubK) throws NoSuchAlgorithmException {//construtor da thread
            this.socket = s1;
            this.group = group1;
            this.value = value;
            this.privateKey = privK;
            this.publicKey = pubK;
            this.chaves = new Integer[6];
            this.valores = new Integer[6];
            this.ordem = 0;//ainda não possui ordem definida
            this.start();
	}
	public void run(){
		try {		 	                 // an echo server

			//String data = in.readUTF();
                        byte[] buffer = new byte[1000];
                        DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
                        String mensagem = new String(messageIn.getData());//recebe a próxima mensagem (próprio Hello)
                        
                        Integer participantes = 1;//começa pensando em si como único participante
                        
                        byte [] m = null; 
                        m = ("Hey,"+"?,"+publicKey.toString()).getBytes();//mensagem de 7 caracteres que envia a ordem(ainda indefinida) e a sua chave pública para o grupo
                        DatagramPacket messageOut = new DatagramPacket(m, m.length, group, 6789);
                        Integer novo = 2;//variável para saber quando parar de alterar a própria ordem (após 2 'Hello's: o próprio e o próximo)
                        
 			while(participantes<5) {		// get messages from others in group
 				messageIn = new DatagramPacket(buffer, buffer.length);
 				socket.receive(messageIn);
                                mensagem = new String(messageIn.getData());//recebe os 'Hello's e 'Hey's enviados no grupo até que todos as 5 threads entrem no grupo
 				System.out.println("Received:" + mensagem);
                                if(mensagem.startsWith("Hey")){//Mensagens de 'Hey' são respostas para 'Hello's(mensagens de entrada)
                                    participantes++;//aumenta o número de participantes
                                    if(novo==1){//se ainda não encontrou o próximo 'Hello' aumenta sua ordem
                                        ordem=ordem+1;
                                    }
                                    if(!mensagem.contains("?")){//salva chaves públicas de threads com ordem definida
                                        chaves[Integer.parseInt(mensagem.substring(4, 5))]=Integer.parseInt(mensagem.substring(6, 7));
                                        
                                    }
                                }else if(mensagem.startsWith("Hello")){//Mensagens de 'Hello' são mensagens de entrada, a primeira enviada por uma thread nova entrando no grupo
                                    if(novo==1){//Se é seu segundo Hello pode parar de aumentar sua ordem
                                        novo=0;
                                        m = ("Hey,"+ordem.toString()+","+publicKey.toString()).getBytes();//atualiza a mensagem a ser enviada para sua mensagem com ordem definida
                                        messageOut = new DatagramPacket(m, m.length, group, 6789);
                                    }else if(novo==2){//se é o 'Hello' dela própria
                                        novo=1;
                                    }
                                    participantes = 0;//reinicia a contagem de participantes para ter certeza que há as 5 threads necessárias
                                    socket.send(messageOut);
                                }
  			}
                        if(novo==1){//para a última thread que entrou e não encontrou nenhum 'Hello' após o seu próprio
                            novo=0;
                            m = ("Hey,"+ordem.toString()+","+publicKey.toString()).getBytes();
                            messageOut = new DatagramPacket(m, m.length, group, 6789);
                        }
                        socket.send(messageOut);
                        for(Integer i=0;i<5;i++){ //manda novamente para ter certeza que todos possuem a tabela de senhas públicas preenchida
                           
                            messageIn = new DatagramPacket(buffer, buffer.length);
                            socket.receive(messageIn);
                            mensagem = new String(messageIn.getData());
                            System.out.println("Received:" + mensagem);
                            if(!mensagem.contains("?")){
                                chaves[Integer.parseInt(mensagem.substring(4, 5))]=Integer.parseInt(mensagem.substring(6, 7));  
                            }
                        }
                        
                        //---------------------------------------------------------//
                        
                        Integer zeroes = 0;//quantidade de zeros enviados nesta fase
                        Integer ones = 0;//quantidade de uns enviados nesta fase
                        Integer majority = 0;//qual o valor predominante (0 ou 1)
                        Integer mult = 0;//quantidade que o valor majoritário apareceu
                        Integer tiebreaker = 0;//desempate criado pelo rei da fase
                        for(Integer phase=1;phase<=2;phase++){//PHASE KING
                            System.out.println("Phase "+phase.toString());
                            //Round 1
                            m = ("Hoy,"+ordem.toString()+","+value.toString()).getBytes();//7 caracteres
                            messageOut = new DatagramPacket(m, m.length, group, 6789);
                            socket.send(messageOut);
                            for(int i=0;i<5;i++){ //manda novamente para ter certeza que todos possuem a tabela de senhas públicas preenchida
                                messageIn = new DatagramPacket(buffer, buffer.length);
                                socket.receive(messageIn);
                                mensagem = new String(messageIn.getData());
                                System.out.println("Received:" + mensagem);
                                valores[Integer.parseInt(mensagem.substring(4, 5))]=Integer.parseInt(mensagem.substring(6, 7));  
                            }
                            
                            //Calculo da maioria
                            zeroes=0;
                            ones=0;
                            majority=0;
                            mult=0;
                            for(int i=1;i<=5;i++){
                                if(valores[i]==0){
                                    zeroes++;
                                }else if(valores[i]==1){
                                    ones++;
                                }
                            }
                            //zero maioria
                            if(zeroes>ones){
                                majority=0;
                                mult=zeroes;
                            //um maioria
                            }else if(ones>zeroes){
                                majority=1;
                                mult=ones;
                            //empate --> default
                            }else{
                                majority=0;
                                mult=zeroes;
                            }
                            
                            //Round 2
                            if(phase==ordem){//parte do rei
                                Integer assinatura = privateKey+phase;
                                m = ("Rei,"+assinatura.toString()+","+majority.toString()).getBytes();//envia a assinatura do rei e a maioria escolhida por ele para servir de desempate
                                messageOut = new DatagramPacket(m, m.length, group, 6789);
                                socket.send(messageOut);
                            }
                            messageIn = new DatagramPacket(buffer, buffer.length);
                            socket.receive(messageIn);
                            mensagem = new String(messageIn.getData());//recebe o tiebreaker
                            System.out.println("Received:" + mensagem);
                            //Desvenda chave privada: o valor recebido = phase + chaveprivada + chavepublica
                            if(Integer.parseInt(mensagem.substring(4,5))+chaves[phase]==phase+5){
                                System.out.println("Rei verificado!");
                                tiebreaker=Integer.parseInt(mensagem.substring(6, 7));
                            }
                            
                            if(mult>((5/2)+1)){//se existe maioria recebe maioria
                                value=majority;
                            }else{//em caso de empate usa o tiebreaker fornecido pelo rei
                                value=tiebreaker;
                            }
                            if(phase==2){//na última fase mostra a decisão tomada pelas threads
                                System.out.println("Decisão: "+value.toString());
                            }
                        }
                        
                        socket.leaveGroup(group);//sai do grupo
                        // read a line of data from the stream
			//out.writeUTF(data);
		}catch (EOFException e){System.out.println("EOF:"+e.getMessage());
		} catch(IOException e) {System.out.println("readline:"+e.getMessage());
		} finally{ socket.close(); /*close failed*/
}
		

	}
}